export const ToggleMenu = () => {
    let isMenuOpen = false;
    document.querySelector("#menu-icon").addEventListener('click', (event) => {
        isMenuOpen = !isMenuOpen;
        event.target.src = isMenuOpen ? '../../assets/icons/close-white.svg' : '../../assets/icons/menu-white.svg';
       document.querySelector("#menu").classList.toggle("display-block");
    });
}