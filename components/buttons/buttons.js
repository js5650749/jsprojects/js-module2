export const addProperty = () => {
  document.querySelector("#button-add").addEventListener('click', (event) => {
      event.target.classList.add("border-extra");
    });
}

export const removeProperty = () => {
    document.querySelector("#button-remove").addEventListener('click', (event) => {
        event.target.classList.remove("border-extra");
    });
}

export const toggleProperty = () => {
    document.querySelector("#button-toggle").addEventListener('click', (event) => {
        event.target.classList.toggle("border-extra");
    });
}

