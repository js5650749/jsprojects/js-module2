import {ToggleMenu} from "./components/navbar/navbar.js";
ToggleMenu();

import {addProperty} from "./components/buttons/buttons.js";
addProperty();

import {removeProperty} from "./components/buttons/buttons.js";
removeProperty();

import {toggleProperty} from "./components/buttons/buttons.js";
toggleProperty();

import {buttonModule} from "./components/buttons/moduleButton.js";
buttonModule();